$(function() {

	var viewportWidth = $(window).width();
	
	$.stellar.positionProperty.custom = {
	
		setTop: function($el, newTop, originalTop) {
			if( $el.data('fading') !== undefined) {				
				if($el.attr('id') == $('#long_text').attr('id')) {
					$el.css('opacity', 1-0.001);
				} else {
					var scrollTop = $(window).scrollTop(),
					elementOffset = $el.closest('div[class="slide"]').offset().top,
					distance = (elementOffset - scrollTop),
					rate = $(window).height() / 2;
					$el.css('opacity', 1 - Math.abs(distance / rate));
				}
			}
			if( $el.data('stellar-moveh-until') !== undefined) {
				var until = $el.attr('data-stellar-moveh-until');
				if($el.attr('data-stellar-ratio') < 0) {
					if(originalTop - newTop > until * $el.parent().width()) {
						$el.css({ 'left': originalTop - newTop });
						if($el.hasClass('big_image')) {
							$el.css({ 'top':  newTop/2.5});
						}
						else {
							$el.css({ 'top':  originalTop});
						}
					} else {
						if($el.hasClass('big_image')) {
							$el.css({ 'top':  newTop/2.5});
						}
						$el.css({ 'left': until*100 + '%' });
					}
				} else {
					if(originalTop - newTop < until * $el.parent().width()) {
						$el.css({ 'left': originalTop - newTop });
						$el.css({ 'top': originalTop });
					} else {
						$el.css({ 'left': until*100 + '%' });
					}
				}
			} else {
				$el.css({ 'top': newTop });
			}
		},

		setLeft: function($el, newLeft, originalLeft) {
			$el.css('left', newLeft);
		}
		
	};
		
	$.stellar({
		positionProperty: 'custom',
		hideDistantElements: false
	});
	
	
    //initialise Stellar.js
    $(window).stellar();

    //Cache some variables
    var links = $('.navigation').find('li');
    slide = $('.slide');
    button = $('.button').add('.button_top');
    mywindow = $(window);
    htmlbody = $('html,body');


    //Setup waypoints plugin
    slide.waypoint(function (event, direction) {

        //cache the variable of the data-slide attribute associated with each slide
        dataslide = $(this).attr('data-slide');
		if(dataslide > 7 && dataslide <= 17) {
			dataslide = 7;
		}
		
		//trick for modifying the previous slide in case several slides pertain to one <li>
		previous = dataslide == 18 ? 7 : dataslide - 1;

        //If the user scrolls up change the navigation link that has the same data-slide attribute as the slide to active and 
        //remove the active class from the previous navigation link 
        if (direction === 'down') {
            $('.navigation li[data-slide="' + dataslide + '"]').addClass('active').siblings().removeClass('active');
        }
        // else If the user scrolls down change the navigation link that has the same data-slide attribute as the slide to active and 
        //remove the active class from the next navigation link 
        else {
            $('.navigation li[data-slide="' + previous + '"]').addClass('active').siblings().removeClass('active');
        }

    });

    //waypoints doesnt detect the first slide when user scrolls back up to the top so we add this little bit of code, that removes the class 
    //from navigation link slide 2 and adds it to navigation link slide 1. 
    mywindow.scroll(function () {
        if (mywindow.scrollTop() == 0) {
            $('.navigation li[data-slide="1"]').addClass('active');
            $('.navigation li[data-slide="2"]').removeClass('active');
        }
    });

    //Create a function that will be passed a slide number and then will scroll to that slide using jquerys animate. The Jquery
    //easing plugin is also used, so we passed in the easing method of 'easeInOutQuint' which is available through the plugin.
    function goToByScroll(dataslide) {
        htmlbody.animate({
            scrollTop: $('.slide[data-slide="' + dataslide + '"]').offset().top + 1
        }, 1500, 'easeInOutQuint');
    }



    //When the user clicks on the navigation links or a button, get the data-slide attribute value of the link and pass that
	//variable to the goToByScroll function
    links.add(button).click(function (e) {
        e.preventDefault();
        dataslide = $(this).attr('data-slide');
        goToByScroll(dataslide);
    });
    
});