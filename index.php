<?php
	require_once('inc/detectdevice.php');
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="Description" content="I'm Diego Alvarado Orellana, engineer, software developer, interaction designer, PhD student. This is my resume."/>
<meta name="keywords" content="Diego, Alvarado, Orellana, UC3M, web developer, HCI, computer, science, UX, resume, cv, curriculum"/>
<title>Diego Alvarado Orellana | Resume</title>

<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.3.0/build/cssreset/reset-min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />

<link rel="image_src" href="http://dei.inf.uc3m.es/dalvarado/myresume/images/slide1/diego.jpg" />
<meta property="og:image" content="http://dei.inf.uc3m.es/dalvarado/myresume/images/slide1/diego.jpg"/>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/jquery.stellar.min.js"></script>
<script type="text/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('require', 'displayfeatures');
  ga('create', 'UA-38079049-2', 'auto');
  ga('send', 'pageview');

</script>
<script>
    $(function(){
        $(window).load(function(){
            $(".scrollbars1").mCustomScrollbar({
				theme:"dark-thick"
			});
			$(".scrollbars2").mCustomScrollbar({
				theme:"light-thick"
			});
        });
    });
</script>
</head>

<body>

	<ul class="navigation">
		<li data-slide="1">Top</li>
		<li data-slide="2">About me</li>
		<li data-slide="3">Work</li>
		<li data-slide="4">Education</li>
		<li data-slide="7">International</li>
		<li data-slide="18">Skills</li>
	</ul>


	<div class="slide" id="slide1" data-slide="1" data-stellar-background-ratio="0.3">
		<div class="title" data-stellar-ratio="2"><h1 class="centered">Diego Alvarado Orellana</h1></div>
		<div class="picture" data-stellar-ratio="5"><div class="centered"></div></div>
	</div><!--End Slide 1-->
	
	
	<div class="slide" id="slide2" data-slide="2" data-stellar-background-ratio="0.5">
		<div id="gradient">
		</div>
		
		<div class="content_text" data-fading="true" data-stellar-ratio="-2" data-stellar-moveh-until="0.1" data-stellar-vertical-offset="-20">
			<p>I'm a lifelong learner. I'm always trying to get my hands dirty by experimenting with new technologies.
			I'm a team worker, and yet I enjoy developing things by myself. My areas of interest are software engineering,
			web and mobile applications design and development, human-computer interaction, UX, interaction design and usability.</p>
			<p>I love traveling and playing tennis. My heroes are Roger Federer and Harry Potter.
			In my spare time, I work on my PhD.</p>
		</div>

		<span class="slideno">About me</span>
		<a class="button" data-slide="3" title=""></a>

	</div><!--End Slide 2-->
	
	
	<div class="slide" id="slide3" data-slide="3">
				
		<div class="content_text scrollbars1" data-fading="true" data-stellar-ratio="1" data-stellar-vertical-offset="0">
			<span class="date">2 November 2010 - Present</span>
			<p class="p_title">Research Assistant</p>
			<span class="date">DEI Interactive Systems Lab - Universidad Carlos III de Madrid (Spain)</span>
			<p>I'm currently working in the area of Human-Computer 
			Interaction (HCI). My research is oriented towards the finding of innovative ways by which children may
			interact with novel technologies to allow them to become part of an adult design team. My main research
			areas in HCI are co-design and interaction design for children.
			<a href="http://dei.inf.uc3m.es/dei_web/dei_web/index.php" target="_blank">Go to the Lab DEI site</a>.</p>
			<br/>
			<span class="date">30 April 2014 - 25 July 2014</span>
			<p class="p_title">Research visitor</p>
			<span class="date">Università della Svizzera Italiana (Switzerland)</span>
			<p>I was invited to a research stay at <a href="http://www.usi.ch" target="_blank">USI</a>
			in the beautiful city of Lugano. I worked on the design and implementation of enhanced eBooks for
			children to help them obtain an engaging experience while reading.</p>
			<br/>
			<span class="date">4 January 2010 - 31 July 2010</span>
			<p class="p_title">Software Developer</p>
			<span class="date">Tec-Intraf, C.A. (Venezuela)</span>
			<p>I worked in the very beginnings of this company. It has now expanded and it's called
			<a href="http://www.vikua.com" target="_blank">ViKua</a>. I worked in the design and implementation
			of a system capable of connecting traffic cameras and traffic lights together 
			and communicating them with a remote central via TCP/IP.</p>
			<br/>
			<span class="date">20 July 2009 - 12 December 2009</span>
			<p class="p_title">Intern</p>
			<span class="date">Tec-Intraf, C.A. (Venezuela)</span>
			<p>Here is where I did my internship and worked on my bachelor thesis:
			"Remote control and synchronization of Chacao Municipality's traffic lights". My work
			set the bases to the execution of the first big project this company had, on which I subsequently worked.
			When presented at my university, this work received the honorable mention "Exceptionally Good Project".</p>
			<br/><br/>
		</div>
		
		<div class="image" data-stellar-ratio="3" data-stellar-vertical-offset="0"></div>

		<span class="slideno">Work experience</span>
		<a class="button" data-slide="4" title=""></a>

	</div><!--End Slide 3-->



	<div class="slide" id="slide4" data-slide="4" data-stellar-background-ratio="0">
		<div class="wrapper">

			
		</div>
		
		<div class="content_text scrollbars2" data-fading="true" data-stellar-ratio="1" data-stellar-vertical-offset="0">
			<span class="date">27 September 2010 - 21 September 2011</span>
			<p class="p_title">MSc in Computer Science and Technology</p>
			<span class="date">Universidad Carlos III de Madrid (Spain)</span>
			<p>Master in computer science, focused on the areas of HCI, UX, interaction design and usability.
			<a href="http://www.uc3m.es" target="_blank">Go to the UC3M site</a>.</p>
			<br/>
			<span class="date">4 August 2008 - 12 December 2008</span>
			<p class="p_title">Academic Exchange</p>
			<span class="date">Tecnológico de Monterrey - ITESM (Mexico)</span>
			<p>Have you heard about the european Erasmus exchange programme? Well, it was like that, except I
			REALLY had to study... a lot! Of course, the parties and alcohol characteristic of Erasmus were also present in the
			latinamerican counterpart. I strongly believe that responsible study and hard work and wild partying and fun
			are NOT mutually exclusive.
			<a href="http://www.itesm.mx" target="_blank">Go to the ITESM site</a>.</p>
			<br/>
			<span class="date">1 September 2004 - 12 December 2009</span>
			<p class="p_title">Electrical Engineer</p>
			<span class="date">Universidad Simón Bolívar (Venezuela)</span>
			<p>You see the background picture? That's the campus of
			<a href="http://www.usb.ve" target="_blank">my university</a>. Awesome! Isn't it? <3</p>
			It was cool. But if I had studied computer science, it would have been even cooler! I realized that the right
			path for me was computer science a bit late (about 3 months before my graduation), that's why I decided to
			make a gentle switch by doing a master in computer science and following up with the PhD programme.
			But hey, no regrets at all! I wouldn't have met some of my current best friends if I hadn't studied
			electrical engineering. Besides, I have powerful knowledge on topics that most of computer scientists have no
			idea, like modern physics, telecommunications and systems control.</p>
			<p>Oh! I forgot to mention, I graduated with distinction <i>Summa Cum Laude</i> and position #1 of the class
			out of 141 students. So, it wasn't that bad after all...</p>
			<br/><br/>
		</div>

		<span class="slideno" data-stellar-ratio="0">Education</span>
		<a class="button" data-slide="7" title="" data-stellar-ratio="0"></a>

	</div><!--End Slide 4-->
	
	<div class="slide" id="slide5" data-slide="5" data-stellar-background-ratio="0">
	
		<span class="slideno" data-stellar-ratio="0">Education</span>
		<a class="button" data-slide="7" title="" data-stellar-ratio="0"></a>
	
	</div><!--End Slide 5-->

	<div class="slide" id="slide6" data-slide="6" data-stellar-background-ratio="0">
		
		<span class="slideno">Education</span>
		<a class="button" data-slide="7" title=""></a>
		
		<div class="big_image" data-stellar-ratio="-1.5" data-stellar-moveh-until="0" data-stellar-vertical-offset="0">
			<span class="slideno2">International experience</span>
			<a class="button" data-slide="7" title=""></a>
		</div>

	</div><!--End Slide 6-->
	
	<div class="slide" id="slide7" data-slide="7" data-stellar-background-ratio="0">
		
		<div class="content_text" data-stellar-ratio="0.6" data-stellar-vertical-offset="0">
		<p>I've lived in Caracas, Venezuela for 6 years</p>
		<p>5 years studying, 1 year working</p>
		</div>
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="9" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 7-->
	
	<div class="slide" id="slide8" data-slide="8" data-stellar-background-ratio="0">
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="9" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 8-->
	
	<div class="slide" id="slide9" data-slide="9" data-stellar-background-ratio="0">
		
		<div class="content_text" data-stellar-ratio="0.7" data-stellar-vertical-offset="0">
		<p>5 months studying in Monterrey, Mexico</p>
		</div>
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="11" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 9-->
	
	<div class="slide" id="slide10" data-slide="10" data-stellar-background-ratio="0">
		
		<div class="big_image" data-stellar-ratio="-1.5" data-stellar-moveh-until="0" data-stellar-vertical-offset="0">
			<span class="slideno2">International experience</span>
			<a class="button" data-slide="11" title=""></a>
		</div>

	</div><!--End Slide 10-->
	
	<div class="slide" id="slide11" data-slide="11" data-stellar-background-ratio="0">
		
		<div class="content_text" data-stellar-ratio="0.7" data-stellar-vertical-offset="0">
		<p>1 month studying in Berlin, Germany</p>
		</div>
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="13" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 11-->
	
	<div class="slide" id="slide12" data-slide="12" data-stellar-background-ratio="0">
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="13" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 12-->
	
	<div class="slide" id="slide13" data-slide="13" data-stellar-background-ratio="0">
		
		<div class="content_text" data-fading="true" data-stellar-ratio="3" data-stellar-vertical-offset="0">
			<p>3 months for a research stay in Lugano, Switzerland</p>
		</div>
		<div class="content_text" data-fading="true" data-stellar-ratio="0.6" data-stellar-vertical-offset="0">
			<p>This is one of the most beautiful cities I've ever seen.</p>
			<p>One of the most boring as well, though...</p>
		</div>
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="16" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 13-->
	
	<div class="slide" id="slide14" data-slide="14" data-stellar-background-ratio="0">
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="16" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 14-->
	
	<div class="slide" id="slide15" data-slide="15" data-stellar-background-ratio="0">
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="16" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 15-->
	
	<div class="slide" id="slide16" data-slide="16" data-stellar-background-ratio="0">
		
		<div class="content_text" data-fading="true" data-stellar-ratio="3" data-stellar-vertical-offset="0">
			<p>And then we have Madrid, Spain</p>
		</div>
		<div class="content_text" data-fading="true" data-stellar-ratio="0.6" data-stellar-vertical-offset="0">
			<p>4 years studying and working in this wonderful city.</p>
			<p>I currently live here.</p>
		</div>
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="18" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 16-->
	
	<div class="slide" id="slide17" data-slide="17" data-stellar-background-ratio="0">
		
		<span class="slideno2" data-stellar-ratio="0">International experience</span>
		<a class="button" data-slide="18" title="" data-stellar-ratio="0"></a>
		
	</div><!--End Slide 17-->
	
	<div class="slide" id="slide18" data-slide="18">
		<div id="gradient">
		</div>
		
		<div id="long_text" class="content_text" data-fading="false" data-stellar-ratio="1">
			<p class="p_title">Job-related skills</p>
			<p>Experience:</p>
			<ul class="bulleted">
			<li>- Extensive experience in OO programming and web development.</li>
			<li>- Intermediate experience in mobile apps development for Android and Firefox OS; Agile software 
			development, REST web services, Git, SVN.</li>
			<li>- Little experience with frameworks Ruby on Rails (Ruby) and Symfony (PHP).</li>
			</ul>			
			<p>Knowledge:</p>
			<ul class="bulleted">
			<li>- Advanced knowledge: HTML5, CSS3, JavaScript, PHP, XML, AJAX (JQuery).</li>
			<li>- Good knowledge: Java, Android SDK, Python, ActionScript 3, C/C++, MySQL, LabView, Matlab.</li>
			<li>- Basic knowledge: Ruby, C#.</li>
			</ul>
			<br/>
			<p class="p_title">Languages</p>
			<ul class="bulleted">
			<li>- Spanish (native).</li>
			<li>- English (full professional proficiency - C1*).</li>
			<li>- German (intermediate proficiency - B2*).</li>
			<li>- Italian (intermediate proficiency - B2*).</li>
			<li>- French (I can hardly say hi, bye, a couple of basic sentences, and sing a pair of songs - A1*).</li>
			</ul>
			<br/>
			<p>I've read Harry Potter and the Philosopher's Stone in 4 languages, namely: spanish, english, german and italian.</p>
			<br/>
			<p>* According to the <a href="http://europass.cedefop.europa.eu/en/resources/european-language-levels-cefr"
			target="_blank">Common European Framework of Reference for Languages</a>.</p>
			<br/>
			<p class="p_title">Other skills</p>
			<ul class="bulleted">
			<li>- Communication skills acquired through studies and membership at the Electrical Engineers' Career
			Committee of the University.</li>
			<li>- Excellent presentation skills gained through my experience as a researcher presenting my work at 
			different conferences in Europe.</li>
			<li>- Proven adaptability to multicultural environments, gained through my studies and work 
			experience abroad.</li>
			<li>- Teamwork spirit.</li>
			<li>- Tennis: before college I played competency-level tennis. I was ranked in the top 20 players of Venezuela
			in my category (best ranking: #12). I carried on playing tennis in college, but the level and the physical
			demand was not the same. I belonged to the tennis team throughout my entire university course (5 years).</li>
			</ul>
			<br/>
			<p class="p_title">Certifications</p>
			<ul class="bulleted">
			<li>- Introduction to Digital Marketing. IAB Spain.</li>
			<li>- Programming Mobile Applications for Android Handheld Systems.
			<a href="https://www.coursera.org/maestro/api/certificate/get_certificate?verify-code=XUYBF3QK7W"
			target="_blank">Verified certificate from Coursera.</a></li>
			<li>- Programming Cloud Services for Android Handheld Systems.
			<a href="https://www.coursera.org/maestro/api/certificate/get_certificate?verify-code=ZDK3M588BD"
			target="_blank">Verified certificate from Coursera.</a></li>
			<li>- Software as a Service
			<a href="https://s3.amazonaws.com/verify.edx.org/downloads/1fde165403e04b68a9b08df9e6d9788b/Certificate.pdf"
			target="_blank">Part I</a> and
			<a href="https://s3.amazonaws.com/verify.edx.org/downloads/cb220f5845514817807bb8f7b1a5bacb/Certificate.pdf"
			target="_blank">Part II</a>. EdX.</li>
			</ul>
		</div>

		<span class="slideno">Skills</span>

	</div><!--End Slide 18-->
	

	<div class="slide" id="slide28" data-slide="28" data-stellar-background-ratio="0.5">
	
		<div class="wrapper">
			
			<a class="button_top" data-slide="1" title=""></a>
			
			<div class="social">
				<a href="https://www.facebook.com/daao87" target="_blank"><img src="images/slide28/Facebook.png" data-stellar-ratio="1" data-stellar-vertical-offset="0" alt=""></a>
				<a href="https://plus.google.com/u/0/+DiegoAlvaradoOrellana/posts" target="_blank"><img src="images/slide28/Googleplus.png" data-stellar-ratio="1" data-stellar-vertical-offset="0" alt=""></a>
				<a href="https://twitter.com/daao87" target="_blank"><img src="images/slide28/Twitter.png" data-stellar-ratio="1" data-stellar-vertical-offset="0" alt=""></a>
				<a href="http://instagram.com/daao87" target="_blank"><img src="images/slide28/Instagram.png" data-stellar-ratio="1" data-stellar-vertical-offset="0" alt=""></a>
				<a href="https://www.linkedin.com/pub/diego-alvarado-orellana/18/242/1a3" target="_blank"><img src="images/slide28/Linkedin.png" data-stellar-ratio="1" data-stellar-vertical-offset="0" alt=""></a>
			</div>
			<div>
				<img src="images/slide28/Email.png" data-stellar-ratio="1" data-stellar-vertical-offset="0" alt="">
			</div>
			<div>
				daao87 (at) gmail (dot) com
			</div>
			<div>
				<a href="http://dei.inf.uc3m.es/dalvarado/" target="_blank">
					My researcher site
				</a>
			</div>
			<div>
				<a href="http://dei.inf.uc3m.es/dalvarado/files/cv_english.pdf" target="_blank">
					My formal resume (pdf)
				</a>
			</div>
			<div class="copyright">
				<div>&copy 2014 Diego Alvarado Orellana</div>
			</div>
		</div>
		
	</div><!--End Slide 28-->

</body>
</html>
